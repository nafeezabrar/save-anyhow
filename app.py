import logging
import time

from watchdog.observers import Observer

from user_config import read_user_config
from event_handlers import DropboxEventHandler
from event_handlers import EmailEventHandler


__author__ = 'nafeez'


def main():
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    user_config = read_user_config()

    # dropbox_access_token = DropboxAuth().authorize() [now using presumed saved access token]
    dropbox_access_token = 'xkxYeUBdxCMAAAAAAAAABYR_lz9ncwtIoeHMJ8j_UCCXtLeKIeTYRYZnwaSfzPlV'
    event_handler_list = [
        DropboxEventHandler(dropbox_access_token),
        EmailEventHandler('email.storage.testing@gmail.com', 'emailstoragetesting')
    ]

    observer = Observer()
    for event_handler in event_handler_list:
        observer.schedule(event_handler, user_config.file_path, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()


if __name__ == '__main__':
    main()

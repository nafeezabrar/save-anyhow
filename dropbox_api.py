import os
from path_util import remove_windows_slash, get_relative_path, get_full_path

__author__ = 'nafeez'

import dropbox
import json
import shelve


class DropboxApi(object):
    def __init__(self, access_token):
        self.access_token = access_token
        self.client = dropbox.client.DropboxClient(access_token)

    def get_all_files(self):
        root = self.client.metadata('/')
        self.get_all_files_recursively(root)
        print(json.dumps(root, sort_keys=True, indent=4))
        storage = shelve.open('file_tree')
        storage = root

    def get_all_files_recursively(self, parent):
        full_path = get_full_path(parent['path'])
        if parent['is_dir']:
            if not os.path.exists(full_path):
                os.makedirs(full_path)
            for child in parent['contents']:
                child_meta = self.client.metadata(child['path'])
                child.update(child_meta)
                self.get_all_files_recursively(child)
        else:
            out = open(full_path, 'wb')
            with self.client.get_file(parent['path']) as f:
                out.write(f.read())


    def get_client_data(self):
        print('linked account: ', self.client.account_info())
        pass

    def add(self, file_path, is_directory):
        if is_directory:
            self.add_folder(file_path)
        else:
            self.add_file(file_path)

    def add_file(self, file_path):
        file_path = remove_windows_slash(file_path)
        print(file_path)
        f = open(file_path, 'r')
        file_path = get_relative_path(file_path)
        response = self.client.put_file(file_path, f)
        print("uploaded:", response)

    def add_folder(self, folder_path):
        folder_path = remove_windows_slash(folder_path)
        print(folder_path)
        folder_path = get_relative_path(folder_path)
        self.client.file_create_folder(folder_path)

    def update(self, file_path):
        file_path = remove_windows_slash(file_path)
        print(file_path)
        f = open(file_path, 'r')
        file_path = get_relative_path(file_path)
        response = self.client.put_file(file_path, f, overwrite=True)
        print("uploaded:", response)

    def delete(self, file_path):
        file_path = remove_windows_slash(file_path)
        print(file_path)
        file_path = get_relative_path(file_path)
        response = self.client.file_delete(file_path)
        print("deleted:", response)

    def move(self):
        pass


if __name__ == '__main__':
    DropboxApi('xkxYeUBdxCMAAAAAAAAABYR_lz9ncwtIoeHMJ8j_UCCXtLeKIeTYRYZnwaSfzPlV').get_all_files()
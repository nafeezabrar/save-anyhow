import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders
import os

class EmailApi(object):
    def __init__(self, account, password):
        self.account = account
        self.password = password
        print(self.account)

    def send_file(self, recipient, file_path):
        file_path = file_path.replace("\\", "/")
        #preparing attachment
        file = open(file_path,'r')
        msg = MIMEMultipart()
        attachment = MIMEBase('application', "octet-stream")
        attachment.set_payload(file.read())
        encoders.encode_base64(attachment)
        attachment.add_header('Content-Disposition', 'attachment', filename=os.path.basename(file_path))
        msg.attach(attachment)
        file.close()
        #preparing sender and recipient
        recipient = 'Email Storage<email.storage.testing@gmail.com>'
        msg['Subject'] = 'The contents of %s' % file_path
        msg['From'] = self.account
        msg['To'] = recipient
        #send the mail
        s = smtplib.SMTP('smtp.gmail.com:587')
        s.set_debuglevel(True)
        s.ehlo()
        s.starttls()
        s.login(self.account, self.password)
        s.sendmail(self.account, [recipient], msg.as_string())
        s.quit()

from watchdog.events import FileSystemEventHandler, FileSystemEvent, FileSystemMovedEvent, EVENT_TYPE_CREATED, \
    EVENT_TYPE_MOVED

from dropbox_api import DropboxApi
from path_util import ensure_string
from email_api import EmailApi

__author__ = 'nafeez'


class BaseEventHandler(FileSystemEventHandler):
    def dispatch(self, event):
        if event.event_type == EVENT_TYPE_MOVED:
            src_path = ensure_string(event.src_path)
            dest_path = ensure_string(event.dest_path)

            if src_path is None:
                event = FileSystemEvent(EVENT_TYPE_CREATED, dest_path, event.is_directory)
            else:
                event = FileSystemMovedEvent(src_path, dest_path, event.is_directory)
        else:
            src_path = ensure_string(event.src_path)
            event = FileSystemEvent(event.event_type, src_path, event.is_directory)

        super().dispatch(event)


class DropboxEventHandler(BaseEventHandler):
    def __init__(self, access_token):
        self.api = DropboxApi(access_token)
        self.api.get_all_files()

    def on_moved(self, event):
        pass

    def on_created(self, event):
        self.api.add(event.src_path, event.is_directory)

    def on_deleted(self, event):
        self.api.delete(event.src_path)

    def on_modified(self, event):
        self.api.update(event.src_path)


class EmailEventHandler(FileSystemEventHandler):
    def __init__(self, account, password):
        self.api = EmailApi(account, password)

    def on_moved(self, event):
        super(EmailEventHandler, self).on_moved(event)

    def on_created(self, event):
        super(EmailEventHandler, self).on_created(event)
        self.api.send_file('email.storage.testing', event.src_path)

    def on_deleted(self, event):
        super(EmailEventHandler, self).on_deleted(event)

    def on_modified(self, event):
        super(EmailEventHandler, self).on_modified(event)


import os

from user_config import read_user_config


__author__ = 'nafeez'


def ensure_string(path):
    if isinstance(path, bytes):
        path = path.decode('utf-8')
    return path


def remove_windows_slash(input_path):
    return input_path.replace('\\', '/')


def get_relative_path(path):
    user_config = read_user_config()
    return os.path.relpath(path, user_config.file_path)


def get_full_path(path):
    user_config = read_user_config()
    return os.path.join(user_config.file_path, path[1:])

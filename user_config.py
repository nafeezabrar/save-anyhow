import configparser

from constant import user_config_file_name


__author__ = 'nafeez'


class UserConfig:
    file_path = ''


USER_PREFERENCE = 'user-preference'
HOME_DIRECTORY = 'home-directory'


def read_user_config():
    parser = configparser.ConfigParser()
    parser.read(user_config_file_name)
    file_path = parser.get(USER_PREFERENCE, HOME_DIRECTORY)

    user_config = UserConfig()
    user_config.file_path = file_path

    return user_config
